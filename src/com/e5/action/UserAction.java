package com.e5.action;

import com.opensymphony.xwork2.ActionSupport;

public class UserAction extends ActionSupport{
	
	public String add() {
		System.out.println("User add...");
		return SUCCESS;
	}
	public String delete() {
		System.out.println("User delete...");
		return SUCCESS;
	}
	public String modify() {
		System.out.println("User modify...");
		return SUCCESS;
	}
}
